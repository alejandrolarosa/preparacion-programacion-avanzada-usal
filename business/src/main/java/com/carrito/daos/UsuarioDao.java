package com.carrito.daos;


import com.carrito.JDBCDriverManager;
import com.carrito.models.Tarjeta;
import com.carrito.models.Usuario;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class UsuarioDao implements Dao<Usuario> {

    @Override
    public List<Usuario> getAll() {
        final EntityManager entityManager = getEntityManager();
        List<Usuario> resultList;
        try {
            resultList = entityManager
                    .createQuery("SELECT a FROM Usuario a ", Usuario.class)
                    .getResultList();
        } finally {
            entityManager.close();
        }
        return resultList;
    }

    public Usuario findUserByDocNumber(String dni) {
        final EntityManager entityManager = getEntityManager();
        Usuario user= null;
        try {
            List<Usuario> resultList = entityManager
                    .createQuery("SELECT a FROM Usuario a WHERE a.dni = :dni", Usuario.class)
                    .setParameter("dni", dni)
                    .getResultList();
            user = resultList.size() > 0 ? resultList.get(0): null;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void save(Usuario user) {
        try {
            merge(user);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void update(Usuario user) {
        merge(user);
    }

    @Override
    public void delete(Usuario user) {

    }

    // 1.E Usar JDBC para obtener las tarjetas de cierto usuario
    public List<Tarjeta> findCardsByUser(String dni) {
        List<Tarjeta> results = new ArrayList<>();
        JDBCDriverManager instance = JDBCDriverManager.getInstance();
        try {
            final Connection connection = instance.connection();
            try(final Statement statement = connection.createStatement()){
                final String query = String.format("SELECT * FROM TARJETA WHERE ID IN (SELECT TARJETAS_ID FROM USUARIO_TARJETA WHERE USUARIO_ID IN (SELECT ID FROM USUARIO WHERE DNI = '%s'))", dni);
                try(ResultSet rs = statement.executeQuery(query)){
                    while(rs.next()){
                        //Retrieve by column name
                        final String id  = rs.getString("ID");
                        final Date fechaVencimiento = rs.getDate("FECHAVENC");
                        final boolean isDebito = rs.getBoolean("ISDEBITO");
                        final String marca = rs.getString("MARCA");
                        final String numero = rs.getString("NUMERO");
                        final Tarjeta e = new Tarjeta(numero, marca, fechaVencimiento, isDebito);
                        results.add(e);
                    }
                }
                finally {
                    statement.close();
                    connection.close();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

}
