package com.carrito.daos;


import com.carrito.daos.exceptions.CardException;
import com.carrito.daos.exceptions.CardNotFoundException;
import com.carrito.models.Tarjeta;
import com.carrito.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

import static com.carrito.daos.Dao.ENTITY_MANAGER_FACTORY;

@Component
public class TarjetaDao {

    @Autowired
    private UsuarioDao usuarioDao;

    public Tarjeta findByNumber(String cardNumber) throws CardNotFoundException {
        final EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        List<Tarjeta> resultList;
        try {
            resultList = em
                    .createQuery("SELECT a FROM Tarjeta a WHERE a.numero = :cardNumber", Tarjeta.class)
                    .setParameter("cardNumber", cardNumber)
                    .getResultList();
        } finally {
            em.close();
        }
        if(resultList.size() > 0){
            return resultList.get(0);
        }
        throw new CardNotFoundException("La tarjeta no se encuentra " + cardNumber);
    }

    public Tarjeta saveCard(String dni, Tarjeta tarjeta) throws CardException {
        final EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        final Usuario user = usuarioDao.findUserByDocNumber(dni);
        if(user == null){
            throw new CardException("El dni no corresponde a un usuario existente.");
        }
        try {
            final EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            final Usuario usuarioFound = em.find(Usuario.class, user.getId());
            em.merge(tarjeta);
            transaction.commit();
            tarjeta.addUsuario(usuarioFound);
            return tarjeta;
        } catch (Exception e){
            e.printStackTrace();
        }
        throw new CardException("No es posible dar de alta la tarjeta con el numero: " + tarjeta.getNumero());
    }

//    public Tarjeta updateDueDate(String cardNumber, Date dueDate) throws CardException {
//        final EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
//        try {
//            final Tarjeta byNumber = findByNumber(cardNumber);
//            final Tarjeta tarjeta = em.find(Tarjeta.class, byNumber.getId());
//            tarjeta.setFechaVenc(dueDate);
//
//            return save(tarjeta);
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//        throw new CardException("No es posible modificar la fecha de vencimiento de la tarjeta con el numero: " + cardNumber);
//    }
}
