package com.carrito.daos.exceptions;

public class CardException extends Exception {

    public CardException(String message) {
        super(message);
    }
}
