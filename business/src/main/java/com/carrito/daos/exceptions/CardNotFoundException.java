package com.carrito.daos.exceptions;

public class CardNotFoundException extends Exception {

    public CardNotFoundException(String message) {
        super(message);
    }
}
