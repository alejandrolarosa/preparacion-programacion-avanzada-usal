package com.carrito.daos;


import com.carrito.JDBCDriverManager;
import com.carrito.models.Producto;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

@Component
public class ProductoDao implements Dao<Producto> {

    @Override
    public List<Producto> getAll() {
        final EntityManager entityManager = getEntityManager();
        List<Producto> resultList;
        try {
            resultList = entityManager
                    .createQuery("SELECT a FROM Producto a ", Producto.class)
                    .getResultList();
        } finally {
            entityManager.close();
        }
        return resultList;
    }

    // 1.D Usar Hibernate para obtener los productos de cierta categoria
    public Optional<Producto> findByCategory(String categoryName) {
        final EntityManager entityManager = getEntityManager();
        try {
            return Optional.of(entityManager
                        .createQuery("SELECT a FROM Producto a WHERE a.categoria.name = :category", Producto.class)
                        .setParameter("category", categoryName)
                        .getSingleResult());
        } finally {
            entityManager.close();
        }
    }


    @Override
    public void save(Producto producto) {
        JDBCDriverManager instance = JDBCDriverManager.getInstance();
        try (Connection conn = instance.connection();
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(producto.toInsert());
            System.out.println("Database insert successfully ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Producto producto) {
        JDBCDriverManager instance = JDBCDriverManager.getInstance();
        try (Connection conn = instance.connection();
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(producto.toUpdate());
            System.out.println("Database updated successfully ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Producto producto) {
        JDBCDriverManager instance = JDBCDriverManager.getInstance();
        try (Connection conn = instance.connection();
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(producto.toDelete());
            System.out.println("Database delete successfully ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
