package com.carrito.daos;

import com.carrito.JDBCDriverManager;
import com.carrito.models.Carrito;
import com.carrito.models.Direccion;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Component
public class CarritoDao implements Dao<Carrito> {

    @Override
    public List<Carrito> getAll() {
        final EntityManager entityManager = getEntityManager();
        List<Carrito> resultList;
        try {
            resultList = entityManager
                    .createQuery("SELECT a FROM Carrito a ", Carrito.class)
                    .getResultList();
        } finally {
            entityManager.close();
        }
        return resultList;
    }

    // 1.E Usar JDBC para obtener los domicilios de envios
    public List<Direccion> findAddressesByUser(String clientId) {
        List<Direccion> results = new ArrayList<>();
        JDBCDriverManager instance = JDBCDriverManager.getInstance();
        try {
            final Connection connection = instance.connection();
            try(final Statement statement = connection.createStatement()){
                final String query = String.format("SELECT * FROM DIRECCION WHERE ID IN (SELECT DIRECCION_ID FROM CARRITO WHERE IDCLIENTE = '%s');", clientId);
                try(ResultSet rs = statement.executeQuery(query)){
                    while(rs.next()){
                        //Retrieve by column name
                        final String id  = rs.getString("ID");
                        final String calle = rs.getString("CALLE");
                        final String numero = rs.getString("NUMERO");
                        final String piso = rs.getString("PISO");
                        final Direccion e = new Direccion(calle, numero, piso);
                        results.add(e);
                    }
                }
                finally {
                    statement.close();
                    connection.close();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    // 1.G usar JDBC para obtener los datos de un carrito en particular
    public List<Carrito> findCarritoById(String id) {
        List<Carrito> results = new ArrayList<>();
        JDBCDriverManager instance = JDBCDriverManager.getInstance();
        try {
            final Connection connection = instance.connection();
            try(final Statement statement = connection.createStatement()){
                final String query = String.format("SELECT * FROM CARRITO WHERE ID = '%s';", id );
                try(ResultSet rs = statement.executeQuery(query)){
                    while(rs.next()){
                        //Retrieve by column name
                        final String ID  = rs.getString("ID");
                        final String estado = rs.getString("ESTADO");
                        final String idCliente = rs.getString("IDCLIENTE");
                        final Carrito e = new Carrito(idCliente, estado, null);
                        results.add(e);
                    }
                }
                finally {
                    statement.close();
                    connection.close();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public void save(Carrito carrito) {
        try {
            merge(carrito);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void update(Carrito carrito) {
        try {
            merge(carrito);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Carrito carrito) {
        try {
            remove(carrito);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
