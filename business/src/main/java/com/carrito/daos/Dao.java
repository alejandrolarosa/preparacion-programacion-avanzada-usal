package com.carrito.daos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

public interface Dao<T> {

    EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("db");

    List<T> getAll();

    void save(T t);

    default void merge(T t){
        try {
            final EntityManager em = getEntityManager();
            final EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.merge(t);
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    default void remove(T t){
        try {
            final EntityManager em = getEntityManager();
            final EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            em.remove(t);
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    void update(T t);

    void delete(T t);

    default EntityManager getEntityManager() {
        return ENTITY_MANAGER_FACTORY.createEntityManager();
    }

}
