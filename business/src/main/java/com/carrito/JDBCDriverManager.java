package com.carrito;

import java.sql.*;

public class JDBCDriverManager {

    private static String driver = "org.hsqldb.jdbcDriver";

    private static JDBCDriverManager instance = null;

    private static String url = "jdbc:hsqldb:file:sueldos;shutdown=true;readonly=true;";
    private static String user = "SA";
    private static String pass= "";


    private JDBCDriverManager() throws ClassNotFoundException {
        Class.forName(driver);

    }

    public static JDBCDriverManager getInstance()  {
        if(instance == null){
            try {
                instance = new JDBCDriverManager();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public  Connection connection() throws SQLException {
        return DriverManager.getConnection(url, user, pass);
    }

}
