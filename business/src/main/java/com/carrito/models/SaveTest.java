package com.carrito.models;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Calendar;

public class SaveTest {

    public static void main(String[] args){
        EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("db");
        final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

        Usuario usuario = new Usuario("asdfasdf", "juan","perez","50123123","juan@gmail.com","1234");
        Usuario usuarioPersited = (Usuario) save(entityManager, usuario);

        Direccion direccion = new Direccion("basualdo", "1290","");
        usuarioPersited.addDireccion(direccion);
        save(entityManager, usuarioPersited);

        Tarjeta tarjeta = new Tarjeta("45121234123409877654", "VISA", Calendar.getInstance().getTime(), false);
        usuarioPersited.addTarjeta(tarjeta);
        save(entityManager, usuarioPersited);

        System.out.println("usuario = [" + usuario + "]");

        Carrito carrito = new Carrito(usuarioPersited.getId(),"PENDIENTE",direccion);

        System.out.println("carrito = [" + save(entityManager, carrito) + "]");
    }

    private static Object save(EntityManager entityManager, Object object) {
        Object result=null;
        try {
            final EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            result = entityManager.merge(object);
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
