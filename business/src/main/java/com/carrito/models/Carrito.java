package com.carrito.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Carrito")
public class Carrito {

    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id= UUID.randomUUID().toString();
    private String idCliente;
    private String estado;
    @OneToOne(cascade = CascadeType.MERGE)
    private Direccion direccion;

    public Carrito(String id) {
        this.id = id;
    }

    public Carrito(){}

    public Carrito(String idCliente, String estado, Direccion direccion) {
        this.idCliente = idCliente;
        this.estado = estado;
        this.direccion = direccion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }
}
