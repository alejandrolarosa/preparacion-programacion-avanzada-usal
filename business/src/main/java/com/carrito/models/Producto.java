package com.carrito.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Producto")
public class Producto {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id= UUID.randomUUID().toString();
    private String nombre ;
    private String descripcion;
    private int stock;
    @OneToOne(cascade = CascadeType.MERGE)
    private Categoria categoria;


    public Producto(String nombre, String descripcion, int stocka) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.stock = stock;
    }



    public Producto() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String toUpdate() {
        return String.format("UPDATE PRODUCTO SET DESCRIPCION='%s' NOMBRE='%s' STOCK=$d WHERE ID='$s' ;", descripcion, nombre, stock, id);
    }

    public String toInsert() {
        return String.format("INSERT INTO PRODUCTO (ID,DESCRIPCION,NOMBRE,STOCK) VALUES('%s','%s','%s',$d);' ;", id, descripcion, nombre, stock);
    }

    /* @return DELETE FROM table_name [WHERE Clause]
     */
    public String toDelete() {
        return String.format("DELETE FROM PRODUCTO WHERE ID='$s' ;", id);
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
//    public Categoria getCategoria() {
//        return categoria;
//    }

//    public void setCategoria(Categoria categoria) {
//        this.categoria = categoria;
//    }
}
