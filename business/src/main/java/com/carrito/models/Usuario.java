package com.carrito.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "Usuario")
public class Usuario {


    @Id @GeneratedValue (generator = "system-uuid")
    @GenericGenerator(name= "system-uuid", strategy = "uuid")
    private String id = UUID.randomUUID().toString();
    private String name;
    private String apellido;
    private String dni;
    private String email;
    private String password;

    @OneToMany(mappedBy = "usuario", fetch = FetchType.EAGER)
    private List<Tarjeta> tarjetas = new ArrayList<>();
    @OneToMany(cascade = CascadeType.MERGE)
    private List<Direccion> direcciones = new ArrayList<>();

    public Usuario(String id, String name, String apellido, String dni, String email, String password) {
        this.id = id;
        this.name = name;
        this.apellido = apellido;
        this.dni = dni;
        this.email = email;
        this.password = password;
    }

    public Usuario() { }

    public void addTarjeta(Tarjeta tarjeta) {
        this.tarjetas.add(tarjeta);
    }

    public void addDireccion(Direccion direccion) {
        this.direcciones.add(direccion);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDni() {
        return dni;
    }
}
