package com.carrito.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

import static com.carrito.daos.Dao.ENTITY_MANAGER_FACTORY;


@Entity
@Table(name= "Tarjeta")
public class Tarjeta {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id= UUID.randomUUID().toString();
    @Column(unique = true)
    private String numero;
    private String marca;
    private Date fechaVenc;
    private Boolean isDebito;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;


    public Tarjeta(String numero, String marca, Date fechaVenc, Boolean isDebito) {
        this.numero = numero;
        this.marca = marca;
        this.fechaVenc = fechaVenc;
        this.isDebito = isDebito;
    }

    public Tarjeta() { }

    public void addUsuario(Usuario usuario) {
        final EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        final EntityTransaction trx = em.getTransaction();
        try {
            trx.begin();
            this.usuario = em.find(Usuario.class, usuario.getId());
            em.merge(this);
            trx.commit();
        } catch (final RuntimeException re) {
            re.printStackTrace();
            if(trx.isActive())  trx.rollback();
            throw re;
        } finally {
            em.clear();
            em.close();
        }
    }



    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Date getFechaVenc() {
        return fechaVenc;
    }

    public void setFechaVenc(Date fechaVenc) {
        this.fechaVenc = fechaVenc;
    }

    public Boolean getDebito() {
        return isDebito;
    }

    public void setDebito(Boolean debito) {
        isDebito = debito;
    }

    public String getId() {
        return id;
    }
}
