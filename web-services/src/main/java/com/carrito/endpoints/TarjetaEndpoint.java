package com.carrito.endpoints;

import com.carrito.daos.TarjetaDao;
import com.carrito.daos.exceptions.CardException;
import com.carrito.daos.exceptions.CardNotFoundException;
import com.carrito.models.Tarjeta;
import https.www_howtodoinjava_com.xml.tarjeta.AltaTarjetaRequest;
import https.www_howtodoinjava_com.xml.tarjeta.AltaTarjetaResponse;
import https.www_howtodoinjava_com.xml.tarjeta.ObtenerTarjetaRequest;
import https.www_howtodoinjava_com.xml.tarjeta.ObtenerTarjetaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Endpoint
public class TarjetaEndpoint
{
    private static final String NAMESPACE_URI = "https://www.howtodoinjava.com/xml/tarjeta";

    @Autowired(required=true)
    private TarjetaDao tarjetaDao;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ObtenerTarjetaRequest")
    @ResponsePayload
    public ObtenerTarjetaResponse obtenerTarjeta(@RequestPayload ObtenerTarjetaRequest request) {
        ObtenerTarjetaResponse response = new ObtenerTarjetaResponse();
        try {
            Tarjeta byNumber = tarjetaDao.findByNumber(request.getNumero());
            https.www_howtodoinjava_com.xml.tarjeta.Tarjeta tarjetaDto = new https.www_howtodoinjava_com.xml.tarjeta.Tarjeta();
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(byNumber.getFechaVenc());
            XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            tarjetaDto.setFechaVenc(date2);
            tarjetaDto.setIsDebito(byNumber.getDebito());
            tarjetaDto.setMarca(byNumber.getMarca());
            tarjetaDto.setNumero(byNumber.getNumero());
            response.setTarjeta(tarjetaDto);
        } catch (CardNotFoundException e) {
            e.printStackTrace();
            response.setError(e.getMessage());
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            response.setError(e.getMessage());
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AltaTarjetaRequest")
    @ResponsePayload
    public AltaTarjetaResponse altaTarjeta(@RequestPayload AltaTarjetaRequest request) {
        AltaTarjetaResponse response = new AltaTarjetaResponse();
        try {
            Tarjeta tarjeta = new Tarjeta();
            tarjeta.setNumero(request.getNumero());
            tarjeta.setDebito(request.isIsDebito());
            tarjeta.setMarca(request.getMarca());
            Date date = request.getFechaVenc().toGregorianCalendar().getTime();
            tarjeta.setFechaVenc(date);
            Tarjeta savedCard = tarjetaDao.saveCard(request.getDniUsuario(), tarjeta);
            https.www_howtodoinjava_com.xml.tarjeta.Tarjeta newTarjeta = new https.www_howtodoinjava_com.xml.tarjeta.Tarjeta();
            newTarjeta.setNumero(savedCard.getNumero());
            newTarjeta.setIsDebito(savedCard.getDebito());
            newTarjeta.setMarca(savedCard.getMarca());
            newTarjeta.setFechaVenc(request.getFechaVenc());
            response.setTarjeta(newTarjeta);
        } catch (CardException e) {
            e.printStackTrace();
            response.setError(e.getMessage());
        }
        return response;
    }
}
